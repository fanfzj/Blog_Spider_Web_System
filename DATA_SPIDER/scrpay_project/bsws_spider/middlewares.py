import random
from settings import USER__AGENT_LIST

class RandomUserAgentMiddleware(object):
    def process_request(self,request,spider):
        rand_use_agent=random.choice(USER__AGENT_LIST)
        if rand_use_agent:
            request.headers.setdefault('User-Agent',rand_use_agent)