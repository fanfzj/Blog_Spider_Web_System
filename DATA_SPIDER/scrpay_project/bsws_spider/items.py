import scrapy
class BlogItem(scrapy.Item):
    title=scrapy.Field()#标题
    contents=scrapy.Field()#内容
    author=scrapy.Field()#作者
    category=scrapy.Field()#分类
    labels=scrapy.Field()#标签
    fbrq=scrapy.Field()#发布日期
    read_num=scrapy.Field()#阅读数
    reply_num=scrapy.Field()#评论数
    href=scrapy.Field()#文章链接
    source=scrapy.Field()#来源