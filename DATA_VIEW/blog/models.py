from django.db import models

# 文章分类
class CategoryModel(models.Model):
    name=models.CharField(max_length=50)
    class Meta:
        db_table='category'
# 作者类
class AuthorModel(models.Model):
    name=models.CharField(max_length=50)
    class Meta:
        db_table='author'
# Create your models here.
class CSDNModel(models.Model):
    title=models.CharField(max_length=100)
    contents=models.TextField()
    author=models.ForeignKey(AuthorModel,on_delete=models.CASCADE)
    category=models.ForeignKey(CategoryModel,on_delete=models.CASCADE)
    labels=models.CharField(max_length=50)
    fbrq=models.DateField()
    read_num=models.IntegerField()
    href=models.CharField(max_length=100)

    class Meta:
        db_table='csdn'

class BlogModel(models.Model):
    title = models.CharField(max_length=100)
    contents = models.TextField()
    author = models.ForeignKey(AuthorModel,on_delete=True)
    category = models.ForeignKey(CategoryModel,on_delete=True)
    labels = models.CharField(max_length=100)
    fbrq = models.DateField()
    read_num = models.IntegerField()
    reply_num=models.IntegerField()
    href = models.CharField(max_length=100)
    source=models.CharField(max_length=255)

    class Meta:
        db_table = 'blog'
        ordering = ['id']

class CnblogsModel(models.Model):
    title = models.CharField(max_length=100)
    contents = models.TextField()
    author = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    labels = models.CharField(max_length=100)
    fbrq = models.DateField()
    read_num = models.IntegerField()
    reply_num=models.IntegerField()
    href = models.CharField(max_length=100)

    class Meta:
        db_table = 'cnblogs'

class JobbleModel(models.Model):
    title = models.CharField(max_length=100)
    contents = models.TextField()
    author = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    labels = models.CharField(max_length=50)
    fbrq = models.DateField()
    href = models.CharField(max_length=100)

    class Meta:
        db_table = 'jobble'