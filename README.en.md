# Blog_Spider_Web_System

#### Description
基于Python Web IT博客管理系统
架构（基于mysql数据库）
1.IT博客数据抓取（爬虫基础模块->scrapy->scrapy-redis）=》（消息队列）
2.博客数据后台管理和前端呈现（（django、flask、tornado）、layui、bootstrap）
3.数据分析、数据挖掘和可视化（pandas、numpy、skleran、tensorflow、echarts）
4.部署（nginx+uwsgi+django/flask、nginx+tornado）

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)