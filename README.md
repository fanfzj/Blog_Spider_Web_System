# Blog_Spider_Web_System

#### 介绍
基于Python Web+Mysql IT博客管理平台


#### 软件架构
1.IT博客数据抓取（爬虫基础模块->scrapy->scrapy-redis）=》（消息队列）
2.博客数据后台管理和前端呈现（（django、flask、tornado）、layui、bootstrap）
3.数据分析、数据挖掘和可视化（pandas、numpy、skleran、tensorflow、echarts）
4.部署（nginx+uwsgi+django/flask、nginx+tornado）


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)